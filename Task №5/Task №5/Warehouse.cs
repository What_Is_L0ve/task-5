﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task__5
{
    public class Warehouse
    {
        private static string[] _weapon;
        public Warehouse()
        {
            _weapon = new string[]
            {
                "Glock 17",
                "Magnum .44",
                "Desert Eagle",
                "Blank"
            };
        }

        public void GetWeapon(Client client)
        {
            if (client is CivilianClient)
            {
                var civilianClient = (CivilianClient)client;
                Console.WriteLine($"Гражданский {civilianClient.Name} запросил(-а) оружие {civilianClient.Weapon}");

                if (civilianClient.WeaponLicense == true)
                    ProcessAwarding(civilianClient);
                else
                    Console.WriteLine("Отсутствует лицензия на оружие\n");
            }
            else
            {
                var militaryClient = (MilitaryClient)client;
                Console.WriteLine($"Военный {militaryClient.Name} запросил оружие {militaryClient.Weapon}");
                ProcessAwarding(militaryClient);
            }

        }
        private static void ProcessAwarding(Client client)
        {
            var random = new Random();
            var countShot = random.Next(2, 6);
            var countHit = random.Next(countShot);
            Console.WriteLine("Проверка на наличие");
            for (var i = 0; i < _weapon.Length; i++)
            {
                if (_weapon[i].Equals(client.Weapon))
                {
                    Console.WriteLine($"{client.Weapon} в наличии");
                    Console.WriteLine($"{client.Name} получил(-а) оружие");
                    for (var j = 1; j <= countShot; j++)
                    {
                        Console.WriteLine($"{client.Name} делает выстрел №{j} из оружия {client.Weapon}");
                    }
                    Console.WriteLine($"{client.Name} закончил(-а) стрельбу с {countHit} попаданиями(-ем) по мишени\n");
                    break;
                }
                else
                {
                    if(_weapon[i] == _weapon[3])
                    {
                        Console.WriteLine("Нет в наличии\n");
                    }
                }
            }            
        }
    }
}
