﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task__5
{
    public class CivilianClient : Client
    {
        public bool WeaponLicense { get; private set; }
        public CivilianClient(string name, string dateBorn, string gender, string weapon, bool weaponLicense)
            : base(name, dateBorn, gender, weapon)
        {
            Name = name;
            DateBorn = dateBorn;
            Gender = gender;
            Weapon = weapon;
            WeaponLicense = weaponLicense;
        }
    }
}
