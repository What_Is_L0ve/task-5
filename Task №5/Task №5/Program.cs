﻿using System;

namespace Task__5
{
    public class Program
    {
        static void Main(string[] args)
        {
            var firstClient = new CivilianClient("Вохменцова Тамара", "30.12.2006", "Ж", "Desert Eagle", false);
            var secondClient = new CivilianClient("Емелин Денис", "21.04.1971", "М", "Magnum .44", true);
            var thirdClient = new MilitaryClient("Понаморёва Александра", "07.10.1993", "Ж", "Glock");
            var fourthClient = new MilitaryClient("Сидоров Владислав", "22.01.1980", "М", "Glock 17");
            var warehouse = new Warehouse();
            
            warehouse.GetWeapon(firstClient);
            warehouse.GetWeapon(secondClient);
            warehouse.GetWeapon(thirdClient);            
            warehouse.GetWeapon(fourthClient);            
        }
    }
}
