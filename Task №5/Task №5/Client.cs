﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task__5
{
    public class Client
    {
        public string Name { get; set; }
        public string DateBorn { get; set; }
        public string Gender { get; set; }
        public string Weapon { get; set; }

        public Client(string name, string dateBorn, string gender, string weapon)
        {
            Name = name;
            DateBorn = dateBorn;
            Gender = gender;
            Weapon = weapon;
        }
    }
}
